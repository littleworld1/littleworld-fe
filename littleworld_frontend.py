"""
LittleWorld Project's frontend_part
"""
import os
import json
from flask import Flask, render_template, jsonify
import requests

backend_python_port = os.getenv('LITTLEWORLD_FRONTEND_PYTHON_PORT', '5002')
backend_host = os.getenv('LITTLEWORLD_BACKEND_SERVICE_NAME', 'localhost')
backend_port = os.getenv('LITTLEWORLD_BACKEND_SERVICE_PORT', '5000')

backend_host_port = 'http://' + backend_host + ':' + backend_port

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def setx_y():
    """Template of an input X and Y page"""
    backend_host_port_xy = backend_host_port + '/setx_y'
    return render_template('SetXY.html', url=backend_host_port_xy)


@app.route('/world', methods=['GET', 'POST'])
def index():
    """Template displaying current epoch etc via API"""
    req = requests.get(backend_host_port + '/api/v1/world')
    response = json.loads(req.content)
    return render_template('index.html', data=response)


@app.route('/health')
def health():
    """Returns status code of 200. Might be extended."""
    resp = jsonify(status_code=200)
    return resp


@app.route('/versions')
def version_front():
    """Returns Frontend version files"""
    versions = {}
    with open('CI_BUILD_DATE') as build_date:
        ci_build_date = build_date.readline()
        versions['ci_build_date'] = ci_build_date
    with open('CI_JOB_ID') as job_id:
        ci_job_id = job_id.readline()
        versions['ci_job_id'] = ci_job_id
    with open('CI_PIPELINE_ID') as pipeline_id:
        ci_pipeline_id = pipeline_id.readline()
        versions['ci_pipeline_id'] = ci_pipeline_id
    with open('CI_COMMIT_REF_NAME') as commit_ref:
        commit_ref_name = commit_ref.readline()
        versions['ci_commit_ref_name'] = commit_ref_name
    with open('CI_COMMIT_SHORT_SHA') as commit_short:
        commit_short_sha = commit_short.readline()
        versions['ci_commit_short_sha'] = commit_short_sha

    return render_template('versions.html', versions=versions)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=int(backend_python_port))
